import Vue from "vue";
import Vuetify from "vuetify/lib";
import "vuetify/src/stylus/app.styl";

Vue.use(Vuetify, {
  iconfont: "md",
  theme: {
    drawer: "#9652ff",
    navbar: "#9ACD32",
    header: "#2e8b57",
    icon: "15BC63"
  }
});
