import Vue from "vue";
import Router from "vue-router";
import Home from "@/components/Home.vue";
import Register from "@/components/Register.vue";
import Login from "@/components/Login.vue";
import Team from "@/components/Team.vue";
import Form from "@/components/Form.vue";
import Type from "@/components/Type.vue";
import Plants from "@/components/Plants.vue";
import Reports from "@/components/Reports.vue";
import Maps from "@/components/Maps.vue";
import Group from "@/components/Group.vue";
import StudentsReports from "@/components/StudentsReports.vue";

Vue.use(Router);

export default new Router({
	mode: "history",
	base: process.env.BASE_URL,
	routes: [
		{
			path: "/",
			name: "home",
			component: Home,
		},
		{
			path: "/register",
			name: "register",
			component: Register,
		},
		{
			path: "/login",
			name: "login",
			component: Login,
		},
		{
			path: "/team",
			name: "team",
			component: Team,
		},
		{
			path: "/form",
			name: "form",
			component: Form,
		},
		{
			path: "/type",
			name: "type",
			component: Type,
		},
		{
			path: "/plants",
			name: "plants",
			component: Plants,
		},
		{
			path: "/reports",
			name: "reports",
			component: Reports,
		},
		{
			path: "/maps",
			name: "maps",
			component: Maps,
		},
		{
			path: "/group",
			name: "group",
			component: Group,
		},
		{
			path: "/students-reports",
			name: "StudentsReports",
			component: StudentsReports,
		},
	],
});
