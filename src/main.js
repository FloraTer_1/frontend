import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import VeeValidate from "vee-validate";
import * as VueGoogleMaps from "vue2-google-maps";

Vue.config.productionTip = false;

var VueCookie = require("vue-cookie");
Vue.use(VueCookie);
Vue.use(VeeValidate);
Vue.use(VueGoogleMaps, {
	load: {
		key: "AIzaSyB_jysZtgEtrLkKcmonU4QrRTMIfBfSX1o",
	},
});

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount("#app");
