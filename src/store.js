/* eslint-disable no-console */
import Vue from "vue";
import Vuex from "vuex";

import createPersistedState from "vuex-persistedstate";
import * as Cookies from "js-cookie";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		token: null,
		isUserLogedIn: false,
		firstName: "",
		lastName: "",
		accountType: "",
		index: "",
		headerTitle: "",
	},
	mutations: {
		setUserData(state, data) {
			state.firstName = data.first_name;
			state.lastName = data.last_name;
			state.index = data.index;
			if (data.user_type == "ST") state.accountType = "student";
			else if (data.user_type == "PROF") state.accountType = "profesor";
			else state.accountType = "nieznany";
		},
		setHeaderTitle(state, data) {
			state.headerTitle = data;
		},

		setToken(state, token) {
			state.token = token;
			if (token) {
				state.isUserLogedIn = true;
			} else {
				state.isUserLogedIn = false;
				state.firstName = "";
				state.lastName = "";
				state.accountType = "";
				state.index = "";
			}
		},
	},
	actions: {
		setToken({ commit }, token) {
			commit("setToken", token);
		},
		setHeaderTitle({ commit }, data) {
			commit("setHeaderTitle", data);
		},
	},
	plugins: [
		createPersistedState({
			storage: {
				getItem: key => Cookies.get(key),
				setItem: (key, value) =>
					Cookies.set(key, value, { expires: 3, secure: false }),
				removeItem: key => Cookies.remove(key),
			},
		}),
	],
});
