# FloraTer_1

## Project setup (install dependencies)

```
npm install -d
```

### Compiles and minifies for production

```
npm run build
```

### Compiles and hot-reloads for development

```
npm run serve
```
